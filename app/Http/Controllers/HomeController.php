<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth()->user();

        // $permission = Permission::create(['name'=>'write articles']);
        // $role = Role::findById(1);
        // $role->givePermissionTo($permission);
        
        // $user->givePermissionTo('write articles');
        // $user->assignRole('writer');
        
        return auth()->user()->permissions;
        return view('home');
    }
}
